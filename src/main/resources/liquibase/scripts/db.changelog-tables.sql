--liquibase formatted sql

--changeset hidai:01
--comment: Create main tables

CREATE TABLE  IF NOT EXISTS Fornecedor (
  idFornecedor INT(11) NOT NULL AUTO_INCREMENT,
  razaoSocial VARCHAR(45) NULL DEFAULT NULL,
  cnpj INT(11) NULL DEFAULT NULL,
  endereco VARCHAR(45) NULL DEFAULT NULL,
  telefone INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (idFornecedor));
  
CREATE TABLE IF NOT EXISTS Produto (
  idProduto INT(11) NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(45) NULL DEFAULT NULL,
  marca VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (idProduto));
  
 CREATE TABLE IF NOT EXISTS Fornecedor_has_Produto (
  Fornecedor_idFornecedor INT(11) NOT NULL,
  Produto_idProduto INT(11) NOT NULL,
  PRIMARY KEY (Fornecedor_idFornecedor, Produto_idProduto),
  INDEX fk_Fornecedor_has_Produto_Produto1_idx (Produto_idProduto ASC),
  INDEX fk_Fornecedor_has_Produto_Fornecedor_idx (Fornecedor_idFornecedor ASC),
  CONSTRAINT fk_Fornecedor_has_Produto_Fornecedor
    FOREIGN KEY (Fornecedor_idFornecedor)
    REFERENCES Fornecedor (idFornecedor)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Fornecedor_has_Produto_Produto1
    FOREIGN KEY (Produto_idProduto)
    REFERENCES Produto (idProduto)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

  CREATE TABLE IF NOT EXISTS Estoque (
  idEstoque INT(11) NOT NULL AUTO_INCREMENT,
  quantidade INT(11) NULL DEFAULT NULL,
  valor INT(11) NULL DEFAULT NULL,
  Produto_idProduto INT(11) NOT NULL,
  PRIMARY KEY (idEstoque, Produto_idProduto),
  INDEX fk_Estoque_Produto1_idx (Produto_idProduto ASC),
  CONSTRAINT fk_Estoque_Produto1
    FOREIGN KEY (Produto_idProduto)
    REFERENCES Produto (idProduto)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE IF NOT EXISTS Funcionario (
  idFuncionario INT(11) NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NULL DEFAULT NULL,
  rg INT(11) NULL DEFAULT NULL,
  cargo VARCHAR(45) NULL DEFAULT NULL,
  telefone INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (idFuncionario));

  CREATE TABLE IF NOT EXISTS Cliente (
  idCliente INT(11) NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NULL DEFAULT NULL,
  cpf INT(11) NULL DEFAULT NULL,
  endereco VARCHAR(45) NULL DEFAULT NULL,
  telefone INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (idCliente));
  
CREATE TABLE IF NOT EXISTS Caixa_venda (
  idCaixa INT(11) NOT NULL AUTO_INCREMENT,
  data DATE NULL DEFAULT NULL,
  valor_total INT(11) NULL DEFAULT NULL,
  quantidade INT(11) NULL DEFAULT NULL,
  Funcionario_idFuncionario INT(11) NOT NULL,
  Cliente_idCliente INT(11) NOT NULL,
  Estoque_idEstoque INT(11) NOT NULL,
  Estoque_Produto_idProduto INT(11) NOT NULL,
  PRIMARY KEY (idCaixa, Funcionario_idFuncionario, Cliente_idCliente, Estoque_idEstoque, Estoque_Produto_idProduto),
  INDEX fk_Caixa_Funcionario1_idx (Funcionario_idFuncionario ASC),
  INDEX fk_Caixa_Cliente1_idx (Cliente_idCliente ASC),
  INDEX fk_Caixa_Estoque1_idx (Estoque_idEstoque ASC, Estoque_Produto_idProduto ASC),
  CONSTRAINT fk_Caixa_Funcionario1
    FOREIGN KEY (Funcionario_idFuncionario)
    REFERENCES Funcionario (idFuncionario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Caixa_Cliente1
    FOREIGN KEY (Cliente_idCliente)
    REFERENCES Cliente (idCliente)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Caixa_Estoque1
    FOREIGN KEY (Estoque_idEstoque , Estoque_Produto_idProduto)
    REFERENCES Estoque (idEstoque , Produto_idProduto)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS USUARIO (
  idUsuario BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  NOME VARCHAR(20) NOT NULL,
  SENHA VARCHAR(50) NOT NULL,
  CONSTRAINT USUARIO_PK PRIMARY KEY (idUsuario),
  CONSTRAINT NOME_UK UNIQUE KEY (NOME)
);

CREATE TABLE IF NOT EXISTS AUTORIZACAO (
  idAutorizacao BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  NOME VARCHAR(45) NOT NULL,
  CONSTRAINT AUTORIZACAO_PK PRIMARY KEY (idAutorizacao),
  CONSTRAINT NOME_UK UNIQUE KEY (NOME)
);

CREATE TABLE IF NOT EXISTS Autorizacao_has_Usuario (
  idUsuario BIGINT UNSIGNED NOT NULL,
  idAutorizacao BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (idUsuario, idAutorizacao),
  FOREIGN KEY USUARIO_FK (idUsuario) REFERENCES USUARIO (idUsuario) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY AUTORIZACAO_FK (idAutorizacao) REFERENCES AUTORIZACAO (idAutorizacao) ON DELETE RESTRICT ON UPDATE CASCADE
);
	
--rollback SET FOREIGN_KEY_CHECKS = 0;
--rollback drop table if exists `historico;
--rollback drop table if exists `livro`;
--rollback drop table if exists `leitor`;
--rollback SET FOREIGN_KEY_CHECKS = 1;
