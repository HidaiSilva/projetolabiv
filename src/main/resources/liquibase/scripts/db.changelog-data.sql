--liquibase formatted sql

--changeset hidai:078
--comment: Data mass to test - Fonecedor
--INSERT INTO Fornecedor (razaoSocial, cnpj, endereco, telefone) VALUES ('FonecedorMaster',12345678,'Rua street',37362874);
--INSERT INTO Fornecedor (razaoSocial, cnpj, endereco, telefone) VALUES ('FonecedorMaster2',12345678,'Rua street',37362874);

--changeset hidai:01
--comment: Data mass to test - Fonecedor
INSERT INTO Fornecedor (razaoSocial, cnpj, endereco, telefone) VALUES ('FonecedorMaster',12345678,'Rua street',37362874);

--changeset hidai:02
--comment: Data mass to test - Produto
INSERT INTO Produto (tipo, marca) VALUES ('pendrive', 'SkanDisk');

--changeset hidai:01_02 03
--comment: Data mass to test - Fonecedor_Produto
INSERT INTO Fornecedor_has_Produto (Fornecedor_idFornecedor, Produto_idProduto) VALUES (1,1);

--changeset hidai:04
--comment: Data mass to test - Estoque
INSERT INTO Estoque (quantidade, valor, Produto_idProduto) VALUES (7, 17, 1);

--changeset hidai:05
--comment: Data mass to test - Funcionario
INSERT INTO Funcionario (nome, rg, cargo, telefone) VALUES ('Jhon', 12345678, 'Gerente', 34569826);

--changeset hidai:06
--comment: Data mass to test - Cliente
INSERT INTO Cliente (nome, cpf, endereco, telefone) VALUES ('Mari', 12345678, 'Rua com fins', 23456977);

--changeset hidai:07
--comment: Data mass to test - Caixa
INSERT INTO Caixa_venda (data, valor_total, quantidade, Funcionario_idFuncionario, Cliente_idCliente, Estoque_idEstoque, Estoque_Produto_idProduto) VALUES (CURDATE(), NULL, NULL, 1, 1, 1, 1);

--changeset hidai:08
--comment: Data mass to test - Usuario
insert into usuario (nome,senha) values ('Jhon','123');

--changeset hidai:09
--comment: Data mass to test - Autorização
insert into autorizacao (nome) values ('role_admin');

--changeset hidai:10
--comment: Data mass to test - AutorizaçãoUsuario
insert into Autorizacao_has_Usuario values (1,1);