package edu.fatec.spring.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Estoque;
import edu.fatec.spring.repositories.EstoqueRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/estoque")
@Transactional
public class EstoqueController {
	@Autowired
	EstoqueRepo estoqueRepository;

	@ApiOperation(value = "Estoques no banco", response = Estoque.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Estoque>> getAll() {
		return new ResponseEntity<Iterable<Estoque>>(estoqueRepository.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Deletar um registro de estoque", response = Estoque.class)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		
		Estoque estoque = estoqueRepository.findById(id);
		if (estoque == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		estoqueRepository.delete(estoque);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Criar um registro de estoque", response = Estoque.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<Estoque> add(@RequestBody Estoque Estoque, UriComponentsBuilder ucBuilder) {
		
		estoqueRepository.save(Estoque);
		return new ResponseEntity<Estoque>(HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Aletar um registro de estoque", response = Estoque.class)
	@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
	public ResponseEntity<Estoque> alterar(@RequestBody Estoque estoque, UriComponentsBuilder ucBuilder) {
		//Estoque.setData(new Date());
		Long cid = estoque.getId();
		Estoque estoqueTemp = new Estoque();
		
		if(estoqueRepository.findById(cid) != null){
			estoqueTemp = estoqueRepository.findOne(cid);
			estoqueTemp.setValor(estoque.getValor());
		}else{
			estoqueRepository.save(estoque);
		}
		return new ResponseEntity<Estoque>(HttpStatus.OK);
	}
	
	
}
