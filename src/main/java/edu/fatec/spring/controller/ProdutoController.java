package edu.fatec.spring.controller;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Produto;
import edu.fatec.spring.repositories.ProdutoRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/produto")
@Transactional

public class ProdutoController {
	@Autowired
	ProdutoRepo produtoRepository;

	@ApiOperation(value = "Todos os fornecedores no banco", response = Produto.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso Successfully retrieved list") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Produto>> getAll() {
		return new ResponseEntity<Iterable<Produto>>(produtoRepository.findAll(), HttpStatus.OK);
	}
	

	@ApiOperation(value = "Pegar produto por ID", response = Produto.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Sucessso Successfully retrieved list"),
			@ApiResponse(code = 404, message = "The resource was not found") })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<Produto> getProdutoID(@PathVariable Long id) {
		Produto prod = produtoRepository.findById(id);
		return (prod != null) ? new ResponseEntity<Produto>(prod, HttpStatus.CREATED)
				: new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value = "Deletar um registro de Produto", response = Produto.class)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		
		Produto Produto = produtoRepository.findById(id);
		if (Produto == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		produtoRepository.delete(Produto);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
		@ApiOperation(value = "Criar um registro de produto", response = Produto.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Produto> add(@RequestBody Produto produto, UriComponentsBuilder ucBuilder) {
		//Produto.setData(new Date());
		produtoRepository.save(produto);

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/produto/{id}").buildAndExpand(Produto.getId()).toUri());
		return new ResponseEntity<Produto>(HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Aletar um registro de Produto", response = Produto.class)
	@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Produto> alterar(@RequestBody Produto produto, UriComponentsBuilder ucBuilder) {
		//Produto.setData(new Date());
		Long cid = produto.getId();
		Produto produtoTemp = new Produto();
		
		if(produtoRepository.findById(cid) != null){
			produtoTemp = produtoRepository.findOne(cid);
			produtoTemp.setTipo(produto.getTipo());
		}else{
			produtoRepository.save(produto);
		}

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/Produto/{id}").buildAndExpand(Produto.getId()).toUri());
		return new ResponseEntity<Produto>(HttpStatus.OK);
	}
	
}
