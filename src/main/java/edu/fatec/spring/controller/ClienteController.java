package edu.fatec.spring.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Cliente;
import edu.fatec.spring.repositories.ClienteRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/cliente")
@Transactional
public class ClienteController {
	@Autowired
	ClienteRepo clienteRepository;

	@ApiOperation(value = "Todos os clientes no banco", response = Cliente.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Cliente>> getAll() {
		return new ResponseEntity<Iterable<Cliente>>(clienteRepository.findAll(), HttpStatus.OK);
	}
	@ApiOperation(value = "Pegar um nome", response = Cliente.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Sucesso"),
			@ApiResponse(code = 404, message = "The resource was not found") })
	@RequestMapping(value = "/get/{nome}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> getClienteNome(@PathVariable String nome) {
		Cliente Cliente = clienteRepository.findByNome(nome);
		return (Cliente != null) ? new ResponseEntity<Cliente>(Cliente, HttpStatus.CREATED)
				: new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value = "Deletar um registro de cliente", response = Cliente.class)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		
		Cliente cliente = clienteRepository.findById(id);
		if (cliente == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		clienteRepository.delete(cliente);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Criar um registro de cliente", response = Cliente.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Cliente> add(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) {
		//cliente.setData(new Date());
		clienteRepository.save(cliente);

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/cliente/{id}").buildAndExpand(cliente.getId()).toUri());
		return new ResponseEntity<Cliente>(HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Aletar um registro de cliente", response = Cliente.class)
	@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Cliente> alterar(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) {
		//cliente.setData(new Date());
		Long cid = cliente.getId();
		Cliente clienteTemp = new Cliente();
		
		if(clienteRepository.findById(cid) != null){
			clienteTemp = clienteRepository.findOne(cid);
			clienteTemp.setNome(cliente.getNome());
		}else{
			clienteRepository.save(cliente);
		}

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/cliente/{id}").buildAndExpand(cliente.getId()).toUri());
		return new ResponseEntity<Cliente>(HttpStatus.OK);
	}

}
