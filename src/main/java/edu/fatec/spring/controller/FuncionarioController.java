package edu.fatec.spring.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Funcionario;
import edu.fatec.spring.repositories.FuncionarioRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/funcionario")
@Transactional
public class FuncionarioController {
	@Autowired
	FuncionarioRepo funcionarioRepository;

	@ApiOperation(value = "Todos os fornecedores no banco", response = Funcionario.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso Successfully retrieved list") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Funcionario>> getAll() {
		return new ResponseEntity<Iterable<Funcionario>>(funcionarioRepository.findAll(), HttpStatus.OK);
	}
	@ApiOperation(value = "Deletar um registro de funcionario", response = Funcionario.class)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		
		Funcionario funcionario = funcionarioRepository.findById(id);
		if (funcionario == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		funcionarioRepository.delete(funcionario);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Criar um registro de funcionario", response = Funcionario.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Funcionario> add(@RequestBody Funcionario funcionario, UriComponentsBuilder ucBuilder) {
		//Funcionario.setData(new Date());
		funcionarioRepository.save(funcionario);

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/funcionario/{id}").buildAndExpand(funcionario.getId()).toUri());
		return new ResponseEntity<Funcionario>(HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Aletar um registro de funcionario", response = Funcionario.class)
	@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Funcionario> alterar(@RequestBody Funcionario funcionario, UriComponentsBuilder ucBuilder) {
		//Funcionario.setData(new Date());
		Long cid = funcionario.getId();
		Funcionario funcionarioTemp = new Funcionario();
		
		if(funcionarioRepository.findById(cid) != null){
			funcionarioTemp = funcionarioRepository.findOne(cid);
			funcionarioTemp.setNome(funcionario.getNome());
		}else{
			funcionarioRepository.save(funcionario);
		}

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/Funcionario/{id}").buildAndExpand(Funcionario.getId()).toUri());
		return new ResponseEntity<Funcionario>(HttpStatus.OK);
	}

}
