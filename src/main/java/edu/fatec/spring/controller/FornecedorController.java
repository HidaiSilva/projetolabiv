package edu.fatec.spring.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Fornecedor;
import edu.fatec.spring.repositories.FornecedorRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/fornecedor")
@Transactional
public class FornecedorController {
	
	@Autowired
	FornecedorRepo fornecedorRepository;

	@ApiOperation(value = "Todos os fornecedores no banco", response = Fornecedor.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso Successfully retrieved list") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Fornecedor>> getAll() {
		return new ResponseEntity<Iterable<Fornecedor>>(fornecedorRepository.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Pegar fornecedor baseado em seu ID", response = Fornecedor.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully retrieved list"),
			@ApiResponse(code = 404, message = "The resource was not found") })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<Fornecedor> getFornecedorID(@PathVariable Long id) {
		Fornecedor fornecedor = fornecedorRepository.findById(id);
		return (fornecedor != null) ? new ResponseEntity<Fornecedor>(fornecedor, HttpStatus.CREATED)
				: new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value = "Deletar um registro de Fornecedor", response = Fornecedor.class)
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		
		Fornecedor Fornecedor = fornecedorRepository.findById(id);
		if (Fornecedor == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		fornecedorRepository.delete(Fornecedor);
	
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Criar um registro de Fornecedor", response = Fornecedor.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Fornecedor> add(@RequestBody Fornecedor fornecedor, UriComponentsBuilder ucBuilder) {
		//Fornecedor.setData(new Date());
		fornecedorRepository.save(fornecedor);

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/Fornecedor/{id}").buildAndExpand(Fornecedor.getId()).toUri());
		return new ResponseEntity<Fornecedor>(HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Aletar um registro de Fornecedor", response = Fornecedor.class)
	@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
	//@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Fornecedor> alterar(@RequestBody Fornecedor fornecedor, UriComponentsBuilder ucBuilder) {
		//Fornecedor.setData(new Date());
		Long cid = fornecedor.getId();
		Fornecedor FornecedorTemp = new Fornecedor();
		
		if(fornecedorRepository.findById(cid) != null){
			FornecedorTemp = fornecedorRepository.findOne(cid);
			FornecedorTemp.setRazaoSocial(fornecedor.getRazaoSocial());
		}else{
			fornecedorRepository.save(fornecedor);
		}

//		HttpHeaders header = new HttpHeaders();
//		header.setLocation(ucBuilder.path("/Fornecedor/{id}").buildAndExpand(Fornecedor.getId()).toUri());
		return new ResponseEntity<Fornecedor>(HttpStatus.OK);
	}

}
