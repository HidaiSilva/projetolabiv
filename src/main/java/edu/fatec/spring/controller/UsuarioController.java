package edu.fatec.spring.controller;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.fatec.spring.model.Usuario;
import edu.fatec.spring.repositories.UsuarioRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/usuario")
@Transactional
public class UsuarioController {
	
	@Autowired
	UsuarioRepo usuarioRepository;
	
	@ApiOperation(value = "Pegar produto por ID", response = Usuario.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Sucessso Successfully retrieved list"),
			@ApiResponse(code = 404, message = "The resource was not found") })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> getUsuarioID(@PathVariable Long id) {
		Usuario prod = usuarioRepository.findById(id);
		return (prod != null) ? new ResponseEntity<Usuario>(prod, HttpStatus.CREATED)
				: new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
	}
	
}

