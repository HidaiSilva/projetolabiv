package edu.fatec.spring.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import edu.fatec.spring.model.Caixa;
import edu.fatec.spring.repositories.CaixaRepo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/caixa")
@Transactional
public class CaixaController {
	@Autowired
	CaixaRepo caixaRepository;

	@ApiOperation(value = "Todos os fornecedores no banco", response = Caixa.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Sucesso") })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Caixa>> getAll() {
		return new ResponseEntity<Iterable<Caixa>>(caixaRepository.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Pegar registro por ID", response = Caixa.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Sucessso Successfully retrieved list"),
			@ApiResponse(code = 404, message = "The resource was not found") })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<Caixa> getCaixaID(@PathVariable Long id) {
		Caixa caixa = caixaRepository.findById(id);
		return (caixa != null) ? new ResponseEntity<Caixa>(caixa, HttpStatus.CREATED)
				: new ResponseEntity<Caixa>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value = "Criar um registro de caixa", response = Caixa.class)
@RequestMapping(value = "/add", method = RequestMethod.POST)
//@PreAuthorize("isAuthenticated()")
public ResponseEntity<Caixa> add(@RequestBody Caixa caixa, UriComponentsBuilder ucBuilder) {
	//Caixa.setData(new Date());
	caixaRepository.save(caixa);

//	HttpHeaders header = new HttpHeaders();
//	header.setLocation(ucBuilder.path("/Caixa/{id}").buildAndExpand(Caixa.getId()).toUri());
	return new ResponseEntity<Caixa>(HttpStatus.CREATED);
}

@ApiOperation(value = "Aletar um registro de Caixa", response = Caixa.class)
@RequestMapping(value = "/alterar", method = RequestMethod.PUT)
//@PreAuthorize("isAuthenticated()")
public ResponseEntity<Caixa> alterar(@RequestBody Caixa caixa, UriComponentsBuilder ucBuilder) {
	//Caixa.setData(new Date());
	Long cid = caixa.getId();
	Caixa caixaTemp = new Caixa();
	
	if(caixaRepository.findById(cid) != null){
		caixaTemp = caixaRepository.findOne(cid);
		caixaTemp.setQuantidade(caixa.getQuantidade());
	}else{
		caixaRepository.save(caixa);
	}

//	HttpHeaders header = new HttpHeaders();
//	header.setLocation(ucBuilder.path("/Caixa/{id}").buildAndExpand(Caixa.getId()).toUri());
	return new ResponseEntity<Caixa>(HttpStatus.OK);
}

}
