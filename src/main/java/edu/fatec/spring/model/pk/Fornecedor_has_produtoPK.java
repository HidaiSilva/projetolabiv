package edu.fatec.spring.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Embeddable
@Data
@SuppressWarnings("serial")
@NoArgsConstructor
public class Fornecedor_has_produtoPK  implements Serializable {
	@NonNull
	@Column(name = "Fornecedor_idFornecedor")
	private Long idFornecedor;
	@NonNull
	@Column(name = "Produto_idProduto")
	private Long idProduto;
}
