package edu.fatec.spring.model;

import javax.persistence.EmbeddedId;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.fatec.spring.model.pk.Fornecedor_has_produtoPK;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel

@Table(name = "fornecedor_has_produto")
@AllArgsConstructor
@NoArgsConstructor
public class Fornecedor_has_produto {
	@EmbeddedId
	private Fornecedor_has_produtoPK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idFornecedor")
	@JoinColumn(name = "idFornecedor", referencedColumnName = "idFornecedor", insertable = false, updatable = false)
	@JsonIgnore
	private Fornecedor fornecedor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idProduto")
	@JoinColumn(name = "idProduto", referencedColumnName = "idProduto", insertable = false, updatable = false)
	@JsonIgnore
	private Produto produto;

}
