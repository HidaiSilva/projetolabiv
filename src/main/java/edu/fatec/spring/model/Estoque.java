package edu.fatec.spring.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel
@Entity
@Table(name = "estoque")
@AllArgsConstructor
@NoArgsConstructor
public class Estoque {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idEstoque")
	private Long id;
	@Column(name = "quantidade")
	private String quantidade;
	@Column(name = "valor")
	private String valor;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Produto_idProduto", referencedColumnName = "idProduto")
	private Produto Produto_idProduto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Produto getProduto_idProduto() {
		return Produto_idProduto;
	}

	public void setProduto_idProduto(Produto produto_idProduto) {
		Produto_idProduto = produto_idProduto;
	}
}
