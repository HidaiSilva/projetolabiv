package edu.fatec.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel
@Entity
@Table(name = "caixa_venda")
@AllArgsConstructor
@NoArgsConstructor
public class Caixa {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idCaixa")
	private Long id;
	@Temporal(TemporalType.DATE)
	@Column(name = "data")
	private Date data;
	@Column(name = "valor_total")
	private int valor_total;
	@Column(name = "quantidade")
	private int quantidade;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Funcionario_idFuncionario", referencedColumnName = "idFuncionario")
	private Funcionario Funcionario_idFuncionario;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Cliente_idCliente", referencedColumnName = "idCliente")
	private Cliente Cliente_idCliente;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Estoque_idEstoque", referencedColumnName = "idEstoque")
	private Estoque Estoque_idEstoque;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Estoque_Produto_idProduto", referencedColumnName = "Produto_idProduto")
	private Estoque Estoque_Produto_idProduto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getValor_total() {
		return valor_total;
	}

	public void setValor_total(int valor_total) {
		this.valor_total = valor_total;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Funcionario getFuncionario_idFuncionario() {
		return Funcionario_idFuncionario;
	}

	public void setFuncionario_idFuncionario(Funcionario funcionario_idFuncionario) {
		Funcionario_idFuncionario = funcionario_idFuncionario;
	}

	public Cliente getCliente_idCliente() {
		return Cliente_idCliente;
	}

	public void setCliente_idCliente(Cliente cliente_idCliente) {
		Cliente_idCliente = cliente_idCliente;
	}

	public Estoque getEstoque_idEstoque() {
		return Estoque_idEstoque;
	}

	public void setEstoque_idEstoque(Estoque estoque_idEstoque) {
		Estoque_idEstoque = estoque_idEstoque;
	}

	public Estoque getEstoque_Produto_idProduto() {
		return Estoque_Produto_idProduto;
	}

	public void setEstoque_Produto_idProduto(Estoque estoque_Produto_idProduto) {
		Estoque_Produto_idProduto = estoque_Produto_idProduto;
	}
	
	
	

}
