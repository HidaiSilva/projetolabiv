package edu.fatec.spring.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.fatec.spring.model.Caixa;

@Repository
public interface CaixaRepo extends CrudRepository<Caixa, Long>{
	Caixa findById(Long id);
	List<Caixa> findAll();
	

}
