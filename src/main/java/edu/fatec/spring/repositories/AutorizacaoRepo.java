package edu.fatec.spring.repositories;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.fatec.spring.model.Autorizacao;

public interface AutorizacaoRepo extends CrudRepository<Autorizacao, Long> {

	public List<Autorizacao> findByNomeContainsIgnoreCase(String nome);
	
}
