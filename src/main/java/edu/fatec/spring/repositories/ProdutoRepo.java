package edu.fatec.spring.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.fatec.spring.model.Produto;

@Repository
public interface ProdutoRepo extends CrudRepository<Produto, Long> {
	Produto findById(Long id);
	List<Produto> findAll();
	//public List<Produto> findAllByOrderByNomeAsc();
	//public Produto findByNome(String marca);
}
