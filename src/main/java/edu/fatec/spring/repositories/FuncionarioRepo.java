package edu.fatec.spring.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.fatec.spring.model.Funcionario;

@Repository
public interface FuncionarioRepo extends CrudRepository<Funcionario, Long> {
	Funcionario findById(Long id);
	List<Funcionario> findAll();

}
