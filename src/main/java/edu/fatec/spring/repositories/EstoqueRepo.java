package edu.fatec.spring.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.fatec.spring.model.Estoque;

@Repository
public interface EstoqueRepo extends CrudRepository<Estoque, Long>{
	Estoque findById(Long id);
	List<Estoque> findAll();
}
