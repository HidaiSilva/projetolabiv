package edu.fatec.spring.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.fatec.spring.model.Usuario;

public interface UsuarioRepo extends CrudRepository<Usuario, Long> {
	
	Usuario findById(Long id);

	public Collection<Usuario> findByNome(String nome);
	
	public Usuario findTop1ByNomeContains(String nome);
	
	public List<Usuario> findByIdGreaterThan(Long id);
	
	public List<Usuario> findByAutorizacoesNome(String nome);
	
}