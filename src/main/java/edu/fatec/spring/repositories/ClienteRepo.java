package edu.fatec.spring.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.fatec.spring.model.Cliente;

@Repository
public interface ClienteRepo extends CrudRepository<Cliente, Long>{
	Cliente findById(Long id);
	List<Cliente> findAll();
	public Cliente findByNome(String nome);
	//public Cliente updateById(Long id, Cliente cliente);
	
//	@Modifying
//	@Query("update Cliente c set c.nome = ?1, c.cpf = ?2, c.endereco = ?3, c.telefone = ?4 where c.id = ?5")
//	void setClienteById(String nome, String cpf, String endereco, String telefone, Long id);
	

}
